<?
	$arFilter=Array("IBLOCK_ID"=>$arParams["IBLOCK_ID"], "ID"=>$arSection["ID"], "GLOBAL_ACTIVE"=>"Y"); /* Определяем данные текущего раздела */
	$uf_list=CIBlockSection::GetList(Array(), $arFilter, false, Array("UF_CAT_METATITLE", "UF_CAT_METADESC", "UF_CAT_METAKEY", "UF_H1" )); /* Получаем meta данные из созданных полей */
	if($uf_value=$uf_list->GetNext()){
		$category_meta=$uf_value;
	}

	if(isset($category_meta["UF_CAT_METATITLE"]) && $category_meta["UF_CAT_METATITLE"]!=""){ /* Если поле Title не пусто, ставим значение meta title для раздела */
		$APPLICATION->SetPageProperty("title", $category_meta["UF_CAT_METATITLE"]);
	}
	if(isset($category_meta["UF_CAT_METADESC"]) && $category_meta["UF_CAT_METADESC"]!=""){ /* Если поле Description не пусто, ставим значение meta description для раздела */
		$APPLICATION->SetPageProperty("description", $category_meta["UF_CAT_METADESC"]);
	}
	if(isset($category_meta["UF_CAT_METAKEY"]) && $category_meta["UF_CAT_METAKEY"]!=""){ /* Если поле Keywords не пусто, ставим значение meta keywords для раздела */
		$APPLICATION->SetPageProperty("keywords", $category_meta["UF_CAT_METAKEY"]);
	}
	if(isset($category_meta["UF_H1"]) && $category_meta["UF_H1"]!=""){ 
		$APPLICATION->SetTitle($category_meta["UF_H1"], true);
	}

	?>